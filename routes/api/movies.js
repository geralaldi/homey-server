/**
 * Created by geraldi on 12/8/15.
 */
var express = require('express');
var Movie = require('../../models/movie');
var router = express.Router();

/* GET home page. */
router.post('/', function (req, res, next) {
    var movie = new Movie();
    movie.name = req.body.name;

    movie.save(function (err) {
        if (err)
            res.send(err);
        res.json({message: "Movie created!"});
    });
});

router.get('/', function (req, res, next) {
    Movie.find(function (err, movies) {
        if (err)
            res.send(err);
        res.json(movies);
    });
});

module.exports = router;
/**
 * Created by geraldi on 1/7/16.
 */
var express = require('express');
var User = require('../../models/user');
var router = express.Router();

// Register new user
router.post('/register', function (req, res, next) {
    var user = new User();
    user = req.body;

    user.save(function (err) {
        if (err) {
            res.send(err);
        } else {
            res.json();
        }
    });
});

module.exports = router;
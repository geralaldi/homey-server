/**
 * Created by geraldi on 12/8/15.
 */
var express = require('express');
var app = express();

//var io = require('../../app.js').io;
//var connectedSockets = require('../../app').connectedSockets;
//exports.io = io;
//exports.connectedSockets = connectedSockets;

var movies = require('./movies');
var users = require('./users');
var devices = require('./devices');

app.use('/movies', movies);
app.use('/user', users);
app.use('/device', devices);

module.exports = app;


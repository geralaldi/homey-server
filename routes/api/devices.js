/**
 * Created by geraldi on 1/7/16.
 */
var express = require('express');
var Device = require('../../models/device');
var connectedSockets = require('./api').connectedSockets;
var router = express.Router();

// Get all devices owned by authenticated user
router.get('/', function (req, res, next) {
    console.log(connectedSockets);
});

// Get device details by ID
router.get('/:id', function (req, res, next) {
    res.send(req.params.id);
});

// Add new IoT device
router.post('/add', function (req, res, next) {

});

// Execute device action
router.post('/:id/action', function (req, res, next) {

});

// Remove device
router.delete('/:id', function (req, res, next) {

});

module.exports = router;
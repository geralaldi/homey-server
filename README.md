#Welcome

##What is this?
This is a repository for my undergraduate thesis project named Homey, an IoT management system application built on top of Node.JS platform.

##So how can I use it?
First you have to make sure that you have these installed on your system :
* 1. Node.JS and NPM
* 2. MongoDB
* 3. Bower
If you already have those, then you're good to go.
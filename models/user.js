/**
 * Created by geraldi on 12/11/15.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    id: String,
    name: String,
    email: String,
    password: String,
    dateCreated: Date,
    lastUpdated: Date
});

module.exports = mongoose.model('User', UserSchema);
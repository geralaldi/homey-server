/**
 * Created by geraldi on 12/8/15.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MovieSchema = new Schema({
    name: String
});

module.exports = mongoose.model('Movie', MovieSchema);
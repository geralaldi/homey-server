/**
 * Created by geraldi on 1/12/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HubSchema = new Schema({
    socketId: String,
    remoteAddress: String,
    dateAdded: Date
});

module.exports = mongoose.model('Hub', HubSchema);
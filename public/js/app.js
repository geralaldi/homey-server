/**
 * Created by geraldi on 12/16/15.
 */
angular.module('homeyApp', ['ngRoute', 'ui.bootstrap'])
    .config(appConfig);

function appConfig($routeProvider) {
    $routeProvider.when('/chat', {
        controller: 'ChatController',
        templateUrl: 'partials/chat.html'
    }).when('/', {
        templateUrl: 'partials/welcome.html'
    }).otherwise({redirectTo: '/'});
}
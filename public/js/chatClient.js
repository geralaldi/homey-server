/**
 * Created by geraldi on 12/7/15.
 */
$(document).ready(function() {
    console.log('opened');
    var socket = io();
    var typingTimer;                //timer identifier
    var doneTypingInterval = 2000;  //time in ms, 5 second for example
    var messageForm = $('#messageForm');

    $('form').submit(function() {
        socket.emit('message', messageForm.val());
        $('#messageForm').val('');
        return false;
    });

    messageForm.on('input', function() {
        clearTimeout(typingTimer);
        socket.emit('typing');
    });

    messageForm.on('keyup', function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    socket.on('typing', function() {
        $('#status').text('typing');
    });

    socket.on('stopTyping', function() {
        $('#status').text('');
    });

    socket.on('message', function(data) {
        $('#messageList').append($('<li>').text(data));
    });

    function doneTyping() {
        socket.emit('stopTyping');
    }
});
/**
 * Created by geraldi on 12/16/15.
 */
(function () {
    angular.module('homeyApp').controller('HeaderController', HeaderController);
    function HeaderController($scope, $location) {
        // Sets the isActive value based on the current URL location
        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };
        $scope.toggleNavbar = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
        };
        $scope.isCollapsed = true;
    }
})();
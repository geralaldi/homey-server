/**
 * Created by geraldi on 12/16/15.
 */

(function () {
    'use strict';

    angular.module('homeyApp').controller('ChatController', ChatController);
    function ChatController($scope, Socket) {
        $scope.formData = {};
        $scope.messages = [];

        $scope.sendChatMessage = function () {
            Socket.emit('message', $scope.formData.message);
            $scope.formData.message = '';
        };

        $scope.textChanged = function () {
            Socket.emit('typing');
        };

        Socket.on('message', function ($data) {
            $scope.messages.push($data);
        });
    }
})();
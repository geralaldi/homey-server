/**
 * Created by geraldi on 12/17/15.
 */
'use strict';
angular.module('myApp.controllers', []).controller('HomePageCtrl', ['$rootScope', '$window', '$scope', '$http', 'globals', '$location', '$anchorScroll', 'resContents', 'resSinglecontents', 'resQuizresult', 'resSearch', 'resCategories', '$filter', '$sce', 'ezfb', 'resLogin', 'resAddstory', function ($rootScope, $window, $scope, $http, globals, $location, $anchorScroll, resContents, resSinglecontents, resQuizresult, resSearch, resCategories, $filter, $sce, ezfb, resLogin, resAddstory) {
    var offset = 0;
    var endpage = false;
    $scope.show = false;
    $('#search').removeClass('hide');
    $scope.Urlweb = globals.baseUrl;
    $scope.createStory = false;
    ezfb.getLoginStatus(function (response) {
        console.log(response);
        if (response.status != 'connected') {
            console.log(response.status != 'connected');
            $scope.fbbutton = true;
        }
        else {
            console.log(response);
            $scope.fbbutton = false;
        }
    });
    console.log('base nya' + window.location.origin + window.location.pathname);
    $scope.loginFacebook = function () {
        console.log('aaaa');
        ezfb.getLoginStatus(function (resp) {
            console.log('ini respnya status');
            console.log(resp);
            if (resp.status != 'connected') {
                ezfb.login(function (res) {
                    console.log(res);
                    if (res.status == 'connected') {
                        var data = {
                            social_token: res.authResponse.accessToken,
                            social_type: 'facebook',
                            social_id: res.authResponse.userID
                        }
                        data = JSON.stringify(data)
                        resLogin.post({}, data, function (result) {
                            if (!result.status) {
                                alert(result.meta.message);
                                ezfb.logout(function (resp_logout) {
                                    console.log(resp_logout);
                                });
                            }
                            else {
                                console.log(result);
                                $scope.fbbutton = false;
                            }
                        });
                    }
                });
            }
            else {
                alert('already connected');
                $scope.fbbutton = false;
            }
        });
    };
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    if ($filter('_uriseg')(2) == 'category' && $filter('_uriseg')(3)) {
        $rootScope.windowTitle = globals.appName + ' - ' + globals.appDesc
        $rootScope.apiDone = false;
        resContents.get({category_id: $filter('_uriseg')(3)}, function (data) {
            document.getElementById("category" + $filter('_uriseg')(3)).setAttribute("style", "background:#ffde7e;font-size:18px");
            $rootScope.apiDone = true;
            $scope.contents = data.contents;
        });
    }
    else if ($filter('_uriseg')(2) == 'search' && $filter('_uriseg')(3)) {
        $rootScope.windowTitle = globals.appName + ' - ' + globals.appDesc
        $rootScope.apiDone = false;
        resSearch.get({keyword: $scope.searchByKeyword}, function (data) {
            $rootScope.apiDone = true;
            $scope.contents = data.search;
        });
    }
    else if ($filter('_uriseg')(2) == 'content' && $filter('_uriseg')(3)) {
        $window.scrollTo(0, 0);
        console.log($filter('_uriseg')(3))
        $rootScope.apiDone = false;
        $scope.quizresultDone = true;
        $scope.show = true;
        $scope.showresult = false;
        resSinglecontents.get({verb: $filter('_uriseg')(3)}, function (data) {
            $rootScope.apiDone = true;
            $rootScope.windowTitle = globals.appName + ' - ' + data.story.title
            $scope.metatitle = data.story.title;
            $scope.metadesc = data.story.description;
            $scope.metaimg = data.story.photo_url.small;
            $scope.metaurl = globals.baseUrl + 'v/' + data.story.story_id;
            $scope.singlecontent = data.story;
            $scope.myModel = {
                Url: globals.baseUrl + 'v/' + data.story.story_id,
                Name: data.story.title,
                ImageUrl: data.story.photo_url.small
            };
            $scope.author = data.story.user
            $scope.cards = data.story.cards;
            $scope.fb_page_id = $filter('_uriseg')(3);
            if ($scope.cards[0]['type'] == 'quiz') {
                $scope.typequiz = true;
                $scope.endquiz = false;
                $scope.quizcard = $scope.cards[0];
                $scope.lengthcard = (Object.keys($scope.cards).length);
                $scope.cardindex = 1;
                $location.hash('quiz-question');
                $anchorScroll();
            }
            else {
                $scope.typequiz = false;
            }
        });
        $scope.nextquiz = function (answer_id) {
            if ($scope.cardindex == 1) {
                localStorage.setItem('answer', JSON.stringify([answer_id]))
                $scope.quizcard = $scope.cards[$scope.cardindex];
                $scope.cardindex = $scope.cardindex + 1;
                $location.hash('quiz-question');
                $anchorScroll();
                if ($scope.lengthcard == 2) {
                    var list = localStorage.getItem('answer');
                    list = JSON.parse(list);
                    var submit = {answer: list}
                    submit = JSON.stringify(submit);
                    console.log(submit);
                    $scope.quizresultDone = false;
                    $location.hash('quiz-question');
                    $anchorScroll();
                    $scope.endquiz = true;
                    resQuizresult.post({verb: $scope.singlecontent.story_id}, submit, function (result) {
                        $scope.quizresultDone = true;
                        console.log(result);
                        $scope.quizresult = result.result;
                    });
                }
            }
            else {
                $scope.showresult = true;
                var list = localStorage.getItem('answer');
                list = JSON.parse(list);
                list.push(answer_id);
                localStorage.setItem('answer', JSON.stringify(list));
                $scope.quizcard = $scope.cards[$scope.cardindex];
                $scope.cardindex = $scope.cardindex + 1;
                console.log($scope.cardindex);
                if ($scope.cardindex == $scope.lengthcard) {
                    var submit = {answer: list}
                    submit = JSON.stringify(submit);
                    console.log(submit);
                    $scope.quizresultDone = false;
                    $location.hash('quiz-question');
                    $anchorScroll();
                    $scope.endquiz = true;
                    resQuizresult.post({verb: $scope.singlecontent.story_id}, submit, function (result) {
                        $scope.quizresultDone = true;
                        console.log(result);
                        $scope.quizresult = result.result;
                    });
                }
                else {
                    $location.hash('quiz-question');
                    $anchorScroll();
                }
            }
        }
        resContents.get({verb: 'recommend', contents_id: 1635}, function (result) {
            $scope.suggests = result.contents;
        });
        resContents.get({verb: $filter('_uriseg')(3) + '/common'}, function (result) {
            $scope.common = result.content.common;
        });
    }
    else if ($filter('_uriseg')(2) == 'create') {
        $rootScope.apiDone = true;
        $scope.createStory = true;
        $scope.removeBtn = "btn hide";
        ezfb.getLoginStatus(function (response) {
            console.log(response);
            if (response.status != 'connected') {
                console.log(response.status != 'connected');
                $location.path('/');
            }
            else {
                console.log('in');
                $scope.createStory = true;
                $scope.fbbutton = false;
            }
        });
        $http.get('https://api.ipify.org?format=json').success(function (response) {
            $scope.user_IP = response.ip;
            console.log(response);
        }).error(function (response) {
            $scope.user_IP = 'error getting IP address'
            console.log(response)
        })
        $scope.parentStory = {};
        $scope.imageUpload = function (element) {
            var reader = new FileReader();
            var fd = new FormData();
            fd.append('media', element.files[0]);
            fd.append('device_identifier', 4);
            fd.append('media_type', 'user_photo');
            fd.append('device_platform', 'web');
            fd.append('user_ip_address', $scope.user_IP);
            $rootScope.apiDone = false;
            $http.post("http://jim.pissue.com/api/v1/media", fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (response) {
                $rootScope.apiDone = true;
                console.log('ini respone');
                console.log(response);
                reader.readAsDataURL(element.files[0]);
                $scope.storyPhotoId = response.media.id;
                $scope.thumbnailCover = response.media.url.small;
                console.log($scope.storyPhotoId);
            }).error(function (response) {
                $rootScope.apiDone = true;
                alert('Something went wrong,' + response)
            })
        }
        $scope.cards = [{type: "image", description: "", source: "", ordering: ""}];
        $scope.cardImageUpload = function (element, card) {
            var reader = new FileReader();
            console.log(card);
            var fd = new FormData();
            fd.append('media', element.files[0]);
            fd.append('device_identifier', 4);
            fd.append('media_type', 'user_photo');
            fd.append('device_platform', 'web');
            fd.append('user_ip_address', $scope.user_IP);
            $rootScope.apiDone = false;
            $http.post("http://jim.pissue.com/api/v1/media", fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (response) {
                $rootScope.apiDone = true;
                console.log('ini respone');
                console.log(response);
                card.photo = response.media.id;
                reader.readAsDataURL(element.files[0]);
                card.image = response.media.url.small;
            }).error(function (response) {
                $rootScope.apiDone = true;
                alert('Something went wrong.')
            })
        }
        $scope.addcard = function () {
            $scope.cards.push({type: "image"})
            if ($scope.cards.length >= 2) {
                $scope.removeBtn = "btn btn-danger"
            }
        }
        $scope.removecard = function () {
            $scope.cards.splice($scope.cards.length - 1, 1);
            console.log($scope.cards.length);
            if ($scope.cards.length == 1) {
                $scope.removeBtn = "btn hide"
            }
        }
        console.log('haha');
        $scope.readySubmit = function () {
            var submit = {
                name: $scope.parentStory.storyTitle,
                description: $scope.parentStory.storyDescription,
                photo_id: $scope.storyPhotoId,
                thumbnail_id: $scope.storyPhotoId,
                user_id: 0,
                cards: $scope.cards
            }
            console.log(JSON.stringify(submit));
            $rootScope.apiDone = false;
            $scope.panel = {};
            resAddstory.post({}, submit, function (result) {
                $rootScope.apiDone = true;
                if (result.status) {
                    console.log('sukses');
                    console.log(result);
                    $scope.panel.heading = "Sukses";
                    $scope.panel.message = "Cerita telah berhasil disimpan";
                    console.log($scope.storyEnd);
                    console.log($scope.panel.message);
                    $('#aftercreate').modal('show');
                }
                else {
                    console.log('gagal');
                    console.log(result);
                    $scope.panel.heading = "Gagal!";
                    $scope.panel.message = response.meta.message;
                    $('#aftercreate').modal('show');
                }
            });
        }
    }
    else {
        $rootScope.apiDone = false;
        resContents.get(function (data) {
            $rootScope.apiDone = true;
            $scope.contents = data.contents;
        });
    }
    resCategories.get(function (data) {
        $scope.categories = data.categories;
    });
    $scope.getIframeSrc = function (videoId) {
        var splitChar = '/'
        var splitIndex = 3;
        videoId = videoId.split(splitChar)[splitIndex];
        return 'https://www.youtube.com/embed/' + videoId;
    };
    $scope.loadMore = function () {
        offset = offset + 20;
        if ($filter('_uriseg')(2) == 'category' && $filter('_uriseg')(3)) {
            if (!endpage) {
                $rootScope.apiDone = false;
                resContents.get({category_id: $filter('_uriseg')(3), limit: 20, offset: offset}, function (data) {
                    $rootScope.apiDone = true;
                    var contents = $scope.contents;
                    $scope.contents = contents.concat(data.contents);
                    if (Object.getOwnPropertyNames(data.contents).length == 1) {
                        endpage = true
                    }
                });
            }
        }
        else if ($filter('_uriseg')(2) == 'search' && $filter('_uriseg')(3)) {
            if (!endpage) {
                $rootScope.apiDone = false;
                resSearch.get({keyword: $filter('_uriseg')(3), limit: 20, offset: offset}, function (data) {
                    $rootScope.apiDone = true;
                    var contents = $scope.contents;
                    $scope.contents = contents.concat(data.search);
                    if (Object.getOwnPropertyNames(data.contents).length == 1) {
                        endpage = true
                    }
                });
            }
        }
        else {
            if (!endpage) {
                $rootScope.apiDone = false;
                resContents.get({limit: 20, offset: offset}, function (data) {
                    $rootScope.apiDone = true;
                    var contents = $scope.contents;
                    $scope.contents = contents.concat(data.contents);
                    if (Object.getOwnPropertyNames(data.contents).length == 1) {
                        endpage = true
                    }
                });
            }
        }
    }
    $scope.find = function () {
        $rootScope.searchByKeyword = $scope.search;
        $location.path('search/' + $scope.search);
    }
    $scope.showContent = function () {
        $scope.show = false;
    }
    $scope.more = function () {
        resContents.get({verb: 'recommend', contents_id: 1635}, function (result) {
            $scope.suggests = result.contents;
        });
    }
    $scope.shareFB = function (id_story) {
        resContents.get({verb: 'share/' + id_story}, function (result) {
        });
    }
    $scope.twitpopup = function (title, url, story_id) {
        var width = 575, height = 400, left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            url = "https://twitter.com/intent/tweet?text=" + title + "%0A" + url + "v/" + story_id, opts = 'status=1' + ',width=' + width + ',height=' + height + ',top=' + top + ',left=' + left;
        window.open(url, 'twitter', opts);
        return false;
    }
}]);

'use strict';
angular.module('myApp', ['ngRoute', 'ngSanitize', 'myApp.filters', 'myApp.services', 'myApp.config', 'myApp.directives', 'myApp.controllers', 'angulike', 'updateMeta', 'ezfb']).config(['$routeProvider', '$locationProvider', '$httpProvider', 'ezfbProvider', function ($routeProvider, $locationProvider, $httpProvider, ezfbProvider) {
    ezfbProvider.setInitParams({appId: '762837787142595', version: 'v2.5'});
    $routeProvider.when('/', {templateUrl: 'partials/layout/index.html', controller: 'HomePageCtrl'});
    $routeProvider.when('/category/:category_id', {
        templateUrl: 'partials/layout/index.html',
        controller: 'HomePageCtrl'
    });
    $routeProvider.when('/content/:content_id/:content_title', {
        templateUrl: 'partials/layout/index.html',
        controller: 'HomePageCtrl'
    });
    $routeProvider.when('/content/:content_id', {
        templateUrl: 'partials/layout/index.html',
        controller: 'HomePageCtrl'
    });
    $routeProvider.when('/search/:keyword', {templateUrl: 'partials/layout/index.html', controller: 'HomePageCtrl'});
    $routeProvider.when('/create', {templateUrl: 'partials/layout/create_story.html', controller: 'HomePageCtrl'});
}]).run(function ($rootScope, $location, globals) {
    $rootScope.facebookAppId = '762837787142595';
    $rootScope.metatest = "hahahaha";
    FastClick.attach(document.body);
    var digests = 0;
    $rootScope.$watch(function () {
        digests++;
    });
    $rootScope.globals = globals;
    $rootScope.windowTitle = globals.appName + ' - ' + globals.appDesc;
    $rootScope.setWindowTitle = function (title) {
        $rootScope.windowTitle = globals.appName + ' - ' + title;
    };
    $rootScope.isActive = function (viewLocation, segment) {
        return viewLocation === $filter('_uriseg')(segment);
    };
    $rootScope.goToUrl = function (url) {
        $location.path(url);
    }
});

'use strict';
angular.module('myApp.services', ['ngResource', 'ngSanitize', '720kb.socialshare']).factory('resContents', ['$resource', 'globals', function ($resource, globals) {
    return $resource(globals.apiBaseUrl + 'contents/:verb', {}, {
        get: {
            method: 'GET',
            params: {category_id: '@category_id', contents_id: '@contents_id', limit: '@limit', offset: '@offset'}
        },
    });
}]).factory('resCategories', ['$resource', 'globals', function ($resource, globals) {
    return $resource(globals.apiBaseUrl + 'categories/:verb', {}, {get: {method: 'GET'},});
}]).factory('resSinglecontents', ['$resource', 'globals', function ($resource, globals) {
    return $resource(globals.apiBaseUrl + 'stories/:verb', {}, {
        get: {
            method: 'GET',
            params: {category_id: '@category_id', contents_id: '@contents_id', limit: '@limit', offset: '@offset'}
        },
    });
}]).factory('resQuizresult', ['$resource', 'globals', function ($resource, globals) {
    return $resource(globals.apiBaseUrl + 'stories/:verb/answer', {}, {
        post: {
            method: 'POST',
            headers: {'Content-Type': 'application/json'}
        },
    });
}]).factory('resSearch', ['$resource', 'globals', function ($resource, globals) {
    return $resource(globals.apiBaseUrl + 'search/contents/:verb', {}, {
        get: {
            method: 'GET',
            params: {keyword: '@keyword', limit: '@limit', offset: '@offset'}
        },
    });
}]).factory('resLogin', ['$resource', 'globals', function ($resource, globals) {
    return $resource('http://jim.pissue.com/api/v1/auth/social', {}, {post: {method: 'POST'},});
}]).factory('resAddstory', ['$resource', 'globals', function ($resource, globals) {
    return $resource('http://jim.pissue.com/api/v1/stories/web_save_json_id', {}, {post: {method: 'POST'},});
}]);

'use strict';
angular.module('myApp.config', []).value('globals', {
    appName: 'Pissue',
    appDesc: 'Issue for People',
    apiBaseUrl: 'http://jim.pissue.com/api/v1.5/',
    baseUrl: 'http://pissue.com/',
});

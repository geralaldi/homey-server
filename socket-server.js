/**
 * Created by geraldi on 12/16/15.
 */
module.exports = function (app) {
    var io = app.io;

    io.on('connection', function (socket) {
        console.log('client connected');

        socket.on('message', function (data) {
            console.log(data);
            io.emit('message', data);
        });

        socket.on('typing', function () {
            console.log('someone is typing');
            io.emit('typing');
        });

        socket.on('stopTyping', function () {
            console.log('stop typing');
            io.emit('stopTyping');
        });

        socket.on('disconnect', function (data) {
            console.log('client disconnected');
        });
    });
};